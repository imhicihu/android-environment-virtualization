# Procedures
* Verify technical status of every device that will be part of the process: hard disk, external hard disk, blu-ray recorder, pen-drive, etc.
* Verify portable hard disk drive free space. Then, check if that capacity meets the space needed
* Verify that any device selected for backup purposes is being formatted in the `NTFS` format
* Check the [speed of the hard disk](https://hdd.userbenchmark.com/Software): append time at discretion along the process, so verify time schedule between both parties
* Attach every involved device in the procedure to a [UPS](https://en.wikipedia.org/wiki/Uninterruptible_power_supply), to prevent power failure
* Verify (and fix) any erroneous computer's date & time
* Verify status of internal batteries (main & target computer)
* Verify if the computer is _under_ password. The same goes for _internal_ Windows session(s) (main & target computer)
* Verify access to internet: 
     - turn `on`/`off` wifi connection 
     - detach / attach any LAN access
* Verify quality of internet status (transfer speed, stability, etc.)
* Verify the health of the disk drive, then defrag it. Both actions can be done with [Defragler](https://www.ccleaner.com/defraggler)
* Verify the health of the external hard disk, then defrag it. Both actions can be done with [Defragler](https://www.ccleaner.com/defraggler)
* Verify the health of the pen drive, then defrag it. Both actions can be done with [Defragler](https://www.ccleaner.com/defraggler)
* Update to the latest definitions of the antivirus resident on the (primary) computer
* Do a full scan with the antivirus on the operating system, files, _etc_. The same goes for the hard disk or pen drive
    - If there are _virus_: notify it!
* Turn `off` `File sharing` during diagnostic, defrag or copy/move of files between devices
* Turn `off` Microsoft Windows's built-in `Firewall` during diagnostic, defrag or copy/move of files between devices
* Turn `off` Microsoft Windows's built-in `Notifications` during diagnostic, defrag or copy/move of files between devices
* Turn `off` Microsoft Windows's built-in `Printer sharing` during diagnostic, defrag or copy/move of files between devices
* Turn `off` Microsoft Windows's built-in `Remote login` during diagnostic, defrag or copy/move of files between devices
* Turn `off` Microsoft Windows's built-in `Screen sharing` during diagnostic, defrag or copy/move of files between devices
* Verify backup's software compatibility between operating systems 
* Verify software compatibility between different operating systems, with a _focus_ between 32 vs 64 bits systems. So, some older software demands only 32 bits environments. So, an option is create a some-kind-of custom [Docker image](https://www.howtoforge.com/tutorial/building-and-publishing-custom-docker-images/) or some kind of _sandboxed_ virtual environment: [VirtualBox](https://www.virtualbox.org/), [Vagrant](https://www.vagrantup.com/), [Windows XP Mode](https://www.microsoft.com/en-us/download/details.aspx?id=8002), etc.
* Verify compatibility between file structure according operating systems's
* Backup custom settings (if it it possible) for those software that is *mandatory* to run in the target computer

# Legal:
* All other trademarks are the property of their respective owners.
* Windows, Windows 10, Windows XP, Windows Vista are registered trademark of Microsoft Corporation in the United States and/or other countries.