## Procedures
- check you got a [Gmail](http://gmail.google.com/) account
- before download BlueStacks, check [system requirements](https://support.bluestacks.com/hc/en-us/articles/360025866511-System-specifications-for-installing-BlueStacks-on-MacOS)
- download and install [BlueStacks](https://www.bluestacks.com/)
- if you get a “System Extension Blocked” pop-up, click on the `Open Security & Privacy` button
- eventually it should open a system privacy settings menu. At the bottom, click the `Allow` button
- once installed, run BlueStacks
- drag and drop your compiled [`apk`](https://en.wikipedia.org/wiki/Android_application_package) file 