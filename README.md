![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* This is for internal use. A kind of "playground" to test our android apps, in cases of security, memory leaks, performance, _et alia_
* This repo is a living document that will grow and adapt over time
    ![mac.jpg](https://bitbucket.org/repo/A6B96Ej/images/2608408148-genymotion-for-mac-screenshot.png)


### What is this repository for? ###

* Quick summary
    - Playground for the android apps generated with a focus on security issues and performance analysis
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - Check [checklist.md](https://bitbucket.org/imhicihu/android-environment-virtualization/src/master/checklist.md)
    - Check [colophon.md](https://bitbucket.org/imhicihu/android-environment-virtualization/src/master/colophon.md)
* Configuration
    - Check [procedures.md](https://bitbucket.org/imhicihu/android-environment-virtualization/src/master/procedures.md)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/android-environment-virtualization/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/android-environment-virtualization/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/android-environment-virtualization/commits/) section for the current status

### Related repositories ###

* Some repositories linked with this project:
     - [Bibliographical hybrid (mobile app)](https://bitbucket.org/imhicihu/bibliographical-hybrid-mobile-app/src/)
     
### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/android-environment-virtualization/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/android-environment-virtualization/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)