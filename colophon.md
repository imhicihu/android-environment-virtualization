## Technical requirements ##

* Hardware
	 - Macbook 15"
	 - Dell notebook 15"
     - _Bandwidth_: Consorcio Saavedra 15
* Software
     - Emulators: 
          - [BlueStacks](https://www.bluestacks.com/): Android emulator on Mac and Windows environment
          - [NoxPlayer](https://www.bignox.com/): Android emulator on Windows environment
          - [AndY](https://www.andyroid.net/): Android emulator for both Windows & Mac
          - [GenyMotion](https://www.genymotion.com/): Android emulator for both Mac & Windows environment
          - [ARChon](https://archon-runtime.github.io/): Run Android Apps in Chrome in OS X, Linux and Windows
     - Virtual environment:
          - [VirtualBox](https://www.virtualbox.org/wiki/Downloads) (software to virtualize one operating system inside another operating systems)
     - GIT (code control)
          - [SourceTree](https://www.sourcetreeapp.com/): source code handler
     - Notes:
          - [Evernote](https://evernote.com/) (note taking and to-do lists)
    - Automation on software installation
          + [Munki](https://www.munki.org/munki/): installation/uninstallation automated of MacOSX software
    - Backup:
          - [Duplicati](https://www.duplicati.com/): (open-source backup software)
